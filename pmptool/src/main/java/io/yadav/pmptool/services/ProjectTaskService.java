package io.yadav.pmptool.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.yadav.pmptool.domain.Backlog;
import io.yadav.pmptool.domain.Project;
import io.yadav.pmptool.domain.ProjectTask;
import io.yadav.pmptool.exceptions.ProjectNotFoundException;
import io.yadav.pmptool.repositories.IBacklogRepository;
import io.yadav.pmptool.repositories.IProjectRepository;
import io.yadav.pmptool.repositories.IProjectTaskRepository;

@Service
public class ProjectTaskService {

	@Autowired
	private IBacklogRepository backlogRepository;

	@Autowired
	private IProjectTaskRepository projectTaskRepository;
	
	@Autowired
	private IProjectRepository projectRepository;

	public ProjectTask addProjectTask(String projectIdentifier, ProjectTask projectTask) {

		try {
			// Exceptions: project not found
			// PTs to be added to a specific project, project != null, BL exists
			Backlog backlog = backlogRepository.findByProjectIdentifier(projectIdentifier);

			// set the bl to pt
			projectTask.setBacklog(backlog);

			// we want our project sequence to be like this: IDPRO-1 IDPRO-2 ...100 101
			Integer BacklogSequence = backlog.getPTSequence();

			// Update the BL SEQUENCE
			BacklogSequence++;

			backlog.setPTSequence(BacklogSequence);

			// Add Sequence to Project Task
			projectTask.setProjectSequence(backlog.getProjectIdentifier() + "-" + BacklogSequence);
			projectTask.setProjectIdentifer(projectIdentifier);

			// INITIAL priority when Status is null
			if (projectTask.getStatus() == "" || projectTask.getStatus() == null) {
				projectTask.setStatus("TO_DO");
			}

			if (projectTask.getPriority() == null) { // In the future we need projectTask.getPriority()== 0 to handle
														// the form
				projectTask.setPriority(3);
			}
			return projectTaskRepository.save(projectTask);
		} catch (Exception e) {
			throw new ProjectNotFoundException("Project Not Found");
		}
	}

	public Iterable<ProjectTask> findBacklogById(String id) {
		Project project = projectRepository.findByProjectIdentifier(id);
		
		if(project==null) {
			throw new ProjectNotFoundException("Project with ID: "+id+" doesnot exist.");
		}
		
		return projectTaskRepository.findByProjectIdentifierOrderByPriority(id);
	}
	
	public ProjectTask findPTByProjectSequence(String backlog_id, String pt_id) {
		
		//Make sure we are searching on the right backlog
		Backlog backlog = backlogRepository.findByProjectIdentifier(backlog_id);
		if(backlog == null) {
			throw new ProjectNotFoundException("Project with ID: "+backlog_id+" doesnot exist.");
		}
		
		//Make sure that our task exist		
		ProjectTask projectTask = projectTaskRepository.findByProjectSequence(pt_id);
		if(projectTask == null) {
			throw new ProjectNotFoundException("Project Task: "+pt_id+" doesnot exist.");
		}
		
		//Make sure that the project/backlog id in the path corresponds to the right project
		if(!projectTask.getProjectIdentifier().equals(backlog_id)) {
			throw new ProjectNotFoundException("Project Task: "+pt_id+" doesnot exist in project"+backlog_id);
		}		
		return projectTask;
	}
		
	//Update the project task
	public ProjectTask updatePTByProjectSequence(ProjectTask updatedTask, String backlog_id, String pt_id) {
		//Find existing project
		ProjectTask projectTask = findPTByProjectSequence(backlog_id, pt_id);
		
		//replace it with update task
		 projectTask = updatedTask;
	     return projectTaskRepository.save(projectTask);
	}
	
	public void deletePTByProjectSequence(String backlog_id, String pt_id) {
		ProjectTask projectTask = findPTByProjectSequence(backlog_id, pt_id);
		
		/** Backlog backlog = projectTask.getBacklog();
		List<ProjectTask> pts = backlog.getProjectTasks();
		pts.remove(projectTask);
		backlogRepository.save(backlog); */
		
		
		
		
		projectTaskRepository.delete(projectTask);
	}
}

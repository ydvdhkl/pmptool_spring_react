package io.yadav.pmptool.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import io.yadav.pmptool.domain.User;
import io.yadav.pmptool.exceptions.UsernameAlreadyExistsExceptions;
import io.yadav.pmptool.repositories.IUserRepository;

@Service
public class UserService {
	
	@Autowired
	private IUserRepository userRepository; 
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	public User saveUser(User newUser) {
		
		try {
			newUser.setPassword(bCryptPasswordEncoder.encode(newUser.getPassword()));
			//Username should me unique (exception)
			newUser.setUsername(newUser.getUsername());
			//Make sure Match Password and confirmPassword
			//We don't persist or show the confirmPassword
			newUser.setConfirmPassword("");
			return userRepository.save(newUser);
			
		}catch(Exception e) {
			
			throw new UsernameAlreadyExistsExceptions("Username'"+ newUser.getUsername() +"'already exist.");
			
		}
		
		
		
	}
}

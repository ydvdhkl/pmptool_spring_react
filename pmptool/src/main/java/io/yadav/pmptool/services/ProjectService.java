package io.yadav.pmptool.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.yadav.pmptool.domain.Backlog;
import io.yadav.pmptool.domain.Project;
import io.yadav.pmptool.exceptions.ProjectIdException;
import io.yadav.pmptool.repositories.IBacklogRepository;
import io.yadav.pmptool.repositories.IProjectRepository;

@Service
public class ProjectService {

	@Autowired
	private IProjectRepository projectRepository;
	
	@Autowired
	private IBacklogRepository backlogRepository;

	public Project saveOrUpdateProject(Project project) {
		try {
			project.setProjectIdentifier(project.getProjectIdentifier().toUpperCase());
			
			if(project.getId()==null) {
				Backlog backlog = new Backlog();
				project.setBacklog(backlog);
				backlog.setProject(project);
				backlog.setProjectIdentifier(project.getProjectIdentifier().toUpperCase());
			}
			
			if(project.getId()!=null) {
				project.setBacklog(backlogRepository.findByProjectIdentifier(project.getProjectIdentifier().toUpperCase()));
			}
			
			return projectRepository.save(project);

		} catch (Exception e) {

			throw new ProjectIdException(
					"Project ID '"+ project.getProjectIdentifier().toUpperCase() +"' already exists");
		}
	}
	
	public Project findProjectByIdentifier(String projectId) {
		Project project = projectRepository.findByProjectIdentifier(projectId.toUpperCase());
		if(project == null){
			throw new ProjectIdException(
					"Project ID '"+ projectId +"' doesn't exists");
		}
		return project;
	}
	
	public Iterable<Project> findAllProjects(){
		return projectRepository.findAll();
	}
	
	public void deleteProjectByIdentifier(String projectId) {
		Project project = projectRepository.findByProjectIdentifier(projectId.toUpperCase());
		if(project == null) {
			throw new ProjectIdException("Cannot delete Project with ID '"+ projectId +"'. This project doesn't exists");
		}
		projectRepository.delete(project);
	}
}

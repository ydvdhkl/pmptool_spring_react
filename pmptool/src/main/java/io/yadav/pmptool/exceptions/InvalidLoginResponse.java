package io.yadav.pmptool.exceptions;

public class InvalidLoginResponse {
	
	private String username;
	private String password;
	
	//If we type only invalid password. It still going to tell invalid Username and password. 
	public InvalidLoginResponse() {
		this.username="Invalid Username";
		this.password="Invalid Password";
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}	
}

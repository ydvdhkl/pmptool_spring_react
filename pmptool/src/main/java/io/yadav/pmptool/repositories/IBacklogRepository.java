package io.yadav.pmptool.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.yadav.pmptool.domain.Backlog;

@Repository
public interface IBacklogRepository extends CrudRepository<Backlog, Long>  {
	
	Backlog findByProjectIdentifier(String Identifier); 

}

package io.yadav.pmptool.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.yadav.pmptool.domain.User;

@Repository
public interface IUserRepository extends CrudRepository<User, Long> {

	User findByUsername(String username);
	
	User getById(Long id);
}

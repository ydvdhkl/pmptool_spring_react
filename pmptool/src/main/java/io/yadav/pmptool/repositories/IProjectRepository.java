package io.yadav.pmptool.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import io.yadav.pmptool.domain.Project;

@Repository
public interface IProjectRepository extends CrudRepository<Project, Long> {

	@Override
	Iterable<Project> findAll();
	
	Project findByProjectIdentifier(String projectId);
}

package security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import io.yadav.pmptool.services.CustomUserDetailsService;
import security.SecurityConstants;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true, prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	// WebSecurityConfigurerAdapter is a class which implements WebSecurityConfigure interface
	//https://stackoverflow.com/questions/31995221/correct-use-of-websecurity-in-websecurityconfigureradapter
	
	 @Autowired
	  private JwtAuthenticationEntryPoint unauthorizedHandler;
	 
	 @Autowired
	 private  CustomUserDetailsService customUserDetailsService;
	 
	 @Autowired
	 private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		 http.cors().and().csrf().disable()
         .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
         .sessionManagement()
         .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
         .and()
         .headers().frameOptions().sameOrigin() //To enable H2 Database
         .and()
         .authorizeRequests()
         .antMatchers(
                 "/",
                 "/favicon.ico",
                 "/**/*.png",
                 "/**/*.gif",
                 "/**/*.svg",
                 "/**/*.jpg",
                 "/**/*.html",
                 "/**/*.css",
                 "/**/*.js"
         ).permitAll()
         .antMatchers(SecurityConstants.SIGN_UP_URL).permitAll()
         .antMatchers(SecurityConstants.H2_CONSOLE).permitAll()
         .anyRequest().authenticated();
	}
	
	 @Override
	 protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	     auth.userDetailsService(customUserDetailsService).passwordEncoder(bCryptPasswordEncoder);
	 }

	@Override
	@Bean(BeanIds.AUTHENTICATION_MANAGER)
	protected AuthenticationManager authenticationManager() throws Exception {
		return super.authenticationManager();
	}
	 
}
